module.exports = {
  presets: [
    '@vue/app'
  ],
  plugins: ["transform-optional-chaining", "@babel/plugin-syntax-optional-chaining"]
}
