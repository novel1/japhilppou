import axios from 'axios';
import { simpleHash } from '../../utils.js';

const api = {
  namespaced: true,

  state: {
    requests: {},
  },

  //----------------------------------------------------
  // Mutations
  mutations: {
    SET_REQ_CACHE(state, payload) {
      // If the request max has been hit, remove the first one.
      if(Object.keys(state.requests).length >= payload.reqCacheMax) {
        delete state.requests[Object.keys(state.requests)[0]];
      }
      const cache = {
        data: payload.response.data,
        headers: payload.response.headers,
      }

      state.requests[payload.cacheKey] = cache;
    }
  },

  //----------------------------------------------------
  // Actions
  actions: {
    get({ commit, state, rootGetters }, payload) {
      return new Promise(async (resolve, reject) => {

        const cacheKey = `vnt_cache_${simpleHash(payload.path)}`;

        if(state.requests[cacheKey] !== undefined) {
          // console.log('Returned cache: ', cacheKey);
          return resolve(state.requests[cacheKey]);
        }

        try {
          let response = await axios.get(rootGetters.wpapi + payload.path);
          // console.log('Returned request: ', cacheKey);

          commit('SET_REQ_CACHE', {
            cacheKey,
            response,
            reqCacheMax: rootGetters.reqCacheMax
          });

          return resolve(response);

        } catch (error) {
          reject(error);
        }
      });
    },
  },

  getters: {
    requestCache: state => state.requests
  }
}

export default api;
