export default {
  namespaced: true,

  state: {
    loading: false,
    loaded: false,
    homeAnchor: false,
    mobileMenuStatus: false,
    showHeaderLogo: false,
  },

  mutations: {
    SET_LOADING(state, payload) {
      state.loading = payload;
    },
    SET_TO_HOME_ANCHOR(state, val) {
      state.homeAnchor = val;
    },
    SET_MOBILE_SATUS(state, val) {
      state.mobileMenuStatus = val;
    },
    SET_SHOW_HEADER_LOGO(state, status) {
      this.state.showHeaderLogo = status;
    }
  },

  actions: {
    get_loading({ commit }, payload) {
      commit('SET_LOADING', payload);
    },
    set_to_home_anchor({ commit }, val) {
      commit('SET_TO_HOME_ANCHOR', val);
    },
    set_show_header_logo({ commit }, val) {
      commit('SET_SHOW_HEADER_LOGO', val);
    }
  },

  getters: {
    loading: state => state.loading,
    loaded: state => state.loaded,
    homeAnchor: state => state.homeAnchor,
    mobileMenuStatus: state => state.mobileMenuStatus,
    showHeaderLogo: state => state.showHeaderLogo,
  },
}
