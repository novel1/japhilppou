
import Vue from 'vue';
import Vuex from 'vuex';
import api from './modules/api';
import event from './modules/event';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    wpapi: process.env.VUE_APP_WPAPI,
    vntapi: process.env.VUE_APP_VNTAPI,
    reqCacheMax: process.env.VUE_APP_REQUEST_CACHE_MAX,
  },

  mutations: {

  },

  getters: {
    wpapi: state => state.wpapi,
    vntapi: state => state.vntapi,
    reqCacheMax: state => state.reqCacheMax,
  },

  modules: {
    api,
    event,
  }
});

export default store;
