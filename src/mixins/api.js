export default {

  methods: {
    async fetchPage(slug) {
      if(!slug) return;

      try {
        const { data } = await this.$store.dispatch('api/get', {
          path: `/pages?slug=${slug}`,
        });

        this.page = data[0] ? data[0] : data;
      }
      catch (error) {
        console.log(error);
      }
    },
    async fetchProjectCategories() {
      try {
        const { data } = await this.$store.dispatch('api/get', {
          path: '/project-categories?filter[orderby]=term_order&per_page=100',
        });

        this.projectCategories = data;
      }
      catch (error) {
        console.log(error);
      }
    },
    async fetchProjectCategoryItems() {
      try {
        const { data } = await this.$store.dispatch('api/get', {
          path: `/projects/?filter[taxonomy]=project_category&filter[term]=${this.$route.params.cat}&per_page=100`,
        });

        this.projectCategoriesItems = data;
      }
      catch (error) {
        console.log(error);
      }
    },
  }
}
