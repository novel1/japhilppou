/* eslint-disable */

// Helper for collection methods to determine whether a collection
// should be iterated as an array or as an object.
// Shalllow property wrapper
  // Is a given value an array?
  // Delegates to ECMA5's native Array.isArray
export const isArray = Array.isArray || function(obj) {
  return toString.call(obj) === '[object Array]';
};

export const is = (type) => {
  return obj => {
    [
      'Arguments',
      'Function',
      'String',
      'Number',
      'Date',
      'RegExp',
      'Error',
      'Symbol',
      'Map',
      'WeakMap',
      'Set',
      'WeakSet'
    ].forEach(t => {
      return toString.call(obj) === '[object ' + name + ']';
    });
  }
}

export const shallowProperty = (key) => {
  return obj => obj == null ? void 0 : obj[key];
};

export const getLength = shallowProperty('length');

export const isArrayLike = (collection) => {
  const length = getLength(collection);
  return typeof length == 'number' && length >= 0 && length <= (Math.pow(2, 53) - 1);
};

// Has own property wrapper
export const objHas = (obj, path) => {
  return obj != null && hasOwnProperty.call(obj, path);
}

// Flatten out an array, either recursively (by default), or just one level.
export const flatten = (input, shallow, strict, output) => {
  output = output || [];
  var idx = output.length;
  for(let i = 0, length = getLength(input); i < length; i++) {
    var value = input[i];
    if (isArrayLike(value) && (isArray(value) || toString.call(value) === '[object Arguments]')) {
      // Flatten current level of array or arguments object.
      if (shallow) {
        var j = 0, len = value.length;
        while (j < len) output[idx++] = value[j++];
      } else {
        flatten(value, shallow, strict, output);
        idx = output.length;
      }
    } else if (!strict) {
      output[idx++] = value;
    }
  }
  return output;
};

/**
 * If the given value is not an array, wrap it in one.
 *
 * @param  {Any} value
 * @return {Array}
 */
export const arrayWrap = (value) => {
  return Array.isArray(value) ? value : [value]
}

/**
 * Slugify a string
 *
 * @param  {string} text
 * @return {string} text
 */
export const slugify = (text) => {
  return text
    .toString()
    .toLowerCase()
    .replace(/\s+/g, '-')
    .replace(/[^\w-]+/g, '')
    .replace(/--+/g, '-')
    .replace(/^-+/, '')
    .replace(/-+$/, '');
};

//-----------------------------------------------------------------------------------
// parse a Link header
// Link:<https://example.org/.meta>; rel=meta
// TODO: simplify!
export const parseLinkHeader = (header) => {
  const linkExp = /<[^>]*>\s*(\s*;\s*[^()<>@,;:"/[\]?={} \t]+=(([^()<>@,;:"/[\]?={} \t]+)|("[^"]*")))*(,|$)/g;
  const paramExp = /[^()<>@,;:"/[\]?={} \t]+=(([^()<>@,;:"/[\]?={} \t]+)|("[^"]*"))/g;

  const linkMatch = header.match(linkExp);
  const rels = {};
  for (let i = 0; i < linkMatch.length; i++) {
    const linkSplit = linkMatch[i].split('>');
    const href = linkSplit[0].substring(1);
    const rel = linkSplit[1];
    const relMatch = rel.match(paramExp);
      for (let j = 0; j < relMatch.length; j++) {
        const relSplit = relMatch[j].split('=');
        const relName = relSplit[1].replace(/["']/g, '');
        rels[relName] = href;
      }
  }
  return rels.next || rels.prev ? rels : null;
}

export const getQueryString = ( name ) => {
  name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  let regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
  let results = regex.exec(location.search);
  return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

export const getFormattedDate = (time) => {
  let date = new Date(time);
  return `${this.months[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()}`;
}

export const simpleHash = (str) => {
  let hash = 0;
  if (str.length == 0) {
      return hash;
  }
  for (let i = str.length; i >= 0; i--) {
      let char = str.charCodeAt(i);
      hash = ((hash<<5)-hash)+char;
      hash = hash & hash; // Convert to 32bit integer
  }
  return hash;
}

export const arrayChunk = (size) => {
  const sets = []
  let i = 0;
  const chunks = this.length / size;

    while(i < chunks){
      sets[i] = this.splice(0, size);
      i++;
    }

    return sets;
}

export const def_seo_title = () => 'J+A Philippou Architects';

export const def_seo_desc = () => 'J+A Philippou architects';
