import axios from 'axios';
import { flatten, parseLinkHeader } from '../utils.js';

export default {
  wp_url: process.env.NODE_ENV === 'production' ? 'https://jap-api.noveldigital.works/wp-json' : 'http://localhost:9099/wp-json',
  wp_rest_base: 'wp/v2',
  vnt_rest_base: 'vnt/v1',
  lang: '',

  // Create an instance for wp rest api calls
  wpApiRequestInstance() {
    return axios.create();
  },

  // Main req instance
  req() {
    const inst = this.wpApiRequestInstance();
    inst.interceptors.response.use((res) => {
      res.data._paging = this.handleWpResponsePaging(res.headers);
      return res;
    },
    (error) => {
      return Promise.reject(error);
    });

    return inst;
  },

  // Get all paginated items flattened in one collection
  // TODO: Not working. Flatten the responses to one collection
  getAll(reqUrl) {
    return this.req().get(reqUrl).then((res) => {
      if(!res.data._paging || !res.data._paging.links || !res.data._paging.links.next) {
        return res;
      }
      return Promise.all([
        res,
        this.getAll(res.data._paging.links.next)
      ]).then((resps) => {
        return flatten(flatten(resps));
      });
    });
  },

  // If there is pagination return values
  // to be stroed in the response data (_paging)
  handleWpResponsePaging(headers) {
    const links = parseLinkHeader(headers.link);
    return links
      ? {
        links: links,
        total: headers['x-wp-total'],
        pages: headers['x-wp-totalpages'],
      }
      : null;
  },

  getProjectCategories() {
    return this.req().get(`${this.wp_url}/${this.wp_rest_base}/project-categories?filter[orderby]=term_order?per_page=100`);
  },

  fetchProjectCategoryItems(category) {
    const query = `?filter[taxonomy]=project_category&filter[term]=${category}`
    return this.req().get(`${this.wp_url}/${this.wp_rest_base}/projects${query}?per_page=100`);
  },

  fetchProductCategories() {
    return this.req().get(`${this.wp_url}/${this.wp_rest_base}/project-categories?filter[orderby]=term_order?per_page=100`);
  },

  fetchPages() {
    return this.req().get(`${this.wp_url}/${this.wp_rest_base}/pages?per_page=100`);
  },

  fetchProjects() {
    return this.req().get(`${this.wp_url}/${this.wp_rest_base}/projects?per_page=100`);
  },

  fetchProject(slug) {
    return this.req().get(`${this.wp_url}/${this.wp_rest_base}/projects?slug=${slug}`);
  }
};
