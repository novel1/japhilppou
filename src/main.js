import Vue from 'vue';
import store from './store';
import VueMeta from 'vue-meta';
import VueScrollTo from 'vue-scrollto';
import VueLazyload from 'vue-lazyload';
import { VueMasonryPlugin } from 'vue-masonry';
import Vuebar from 'vuebar';
import * as VueGoogleMaps from 'vue2-google-maps'
import App from './App.vue';
import router from './router';

Vue.config.productionTip = false;
Vue.config.devtools = true;
Vue.config.performance = true;

Vue.use(VueLazyload);
Vue.use(VueMasonryPlugin);
Vue.use(Vuebar);
Vue.use(VueScrollTo);
Vue.use(VueMeta);

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyDTir27b3EblMzROiGvrQkfVbJbWfoEHUM',
    libraries: 'places', // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)

    //// If you want to set the version, you can do so:
    // v: '3.26',
  },

  //// If you intend to programmatically custom event listener code
  //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
  //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
  //// you might need to turn this on.
  // autobindAllEvents: false,

  //// If you want to manually install components, e.g.
  //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
  //// Vue.component('GmapMarker', GmapMarker)
  //// then disable the following:
  // installComponents: true,
})

// You can also pass in the default options
// Vue.use(VueScrollTo, {
//      container: "body",
//      duration: 500,
//      easing: "ease",
//      offset: 0,
//      cancelable: true,
//      onStart: false,
//      onDone: false,
//      onCancel: false,
//      x: false,
//      y: true
//  })

// Add a scroll event directive
Vue.directive('scroll', {
  inserted: function (el, binding) {
    let f = function (evt) {
      if (binding.value(evt, el)) {
        window.removeEventListener('scroll', f);
      }
    }
    window.addEventListener('scroll', f);
  }
});

// Let's go
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
