import Vue from 'vue';
const EventBus = new Vue({
  data: {
    scrolling: false,
    scrollUp: false,
    scrollDown: false,
    scrolled: false,
    scrolledTop: true,
    lastScrollOffset: 0,
  },

  created() {
    this.scrollUp = window.scrollY < 1;
    this.scrollDown = !this.scrollUp;

    this.$emit('scroll', {
      up: this.scrollUp,
      down: this.scrollDown,
      top: this.scrolledTop = window.scrollY < 1,
      lastScrollOffset: this.lastScrollOffset,
      scrollY: window.scrollY,
      evt: null
    });

    window.addEventListener('scroll', this.handleScroll);
  },

  destroyed() {
    window.removeEventListener('scroll', this.handleScroll);
  },

  methods: {
    handleScroll(evt) {

      const data = {
        up: false,
        down: false,
        top: this.scrolledTop = window.scrollY < 1,
        lastScrollOffset: this.lastScrollOffset,
        pageYOffset: window.pageYOffset,
        evt: evt
      };

      const pageOffset = window.pageYOffset || document.documentElement.scrollTop;

      if(pageOffset > 430 && !data.top) {
        this.$emit('pageScrollThreshold', true);
      } else {
        this.$emit('pageScrollThreshold', false);
      }

      if (pageOffset > this.lastScrollOffset){
        this.scrollUp = false;
      } else {
        this.scrollUp = true;
      }

      this.scrollDown = !this.scrollUp;

      this.lastScrollOffset = pageOffset <= 0 ? 0 : pageOffset;

      data.up = this.scrollUp;
      data.down = this.scrollDown;
      data.lastScrollOffset = this.lastScrollOffset;
      data.pageYOffset = window.pageYOffset;

      this.$emit('scrolling', data);
    }
  }
});
export default EventBus;
