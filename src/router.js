import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  // eslint-disable-next-line
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  },
  routes: [
    // 404
    {
      path: '/404',
      name: 'four-o-four',
      component: () => import('./views/FourOFourView.vue'),
    },
    {
      path: '/',
      name: 'home',
      component: () => import('./views/HomeView.vue'),
      meta: {
        hiddenLogo: false,
      }
    },

    // Pages / Posts / Single cpt
    {
      path: '/:slug',
      name: 'page',
      component: () => import('./views/PageView.vue'),
      meta: {
        type: 'page',
      }
    },

    // Projects
    {
      path: '/projects/:cat?',
      name: 'projects.cat',
      component: () => import('./views/ProjectCategoryView.vue')
    },
    {
      path: '/projects/:cat/:project?',
      name: 'project',
      component: () => import('./views/ProjectView.vue')
    },

    {
      path: '*',
      redirect: '/404'
    }
  ],
});

export default router;
